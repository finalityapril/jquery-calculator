//Session 40 Capstone - jQuery

let numberA = null;
let numberB = null;
let operation = null;

$('.btn-numbers').click(function() {
	let txtInputDisplay = $('#txt-input-display').val();
	let txtPlusClick = txtInputDisplay + $(this).html();
	$('#txt-input-display').val(txtPlusClick);
});

$('#btn-add').click(function() {
	if (numberA == null) {
		numberA = $('#txt-input-display').val();
		operation = 'Addition';
		$('#txt-input-display').val(null);
	}
	else if (numberB == null) {
		numberB = $('#txt-input-display').val();
		operation = 'Addition';
		numberB = null;
		$('#txt-input-display').val(null);
	}
});
$('#btn-subtract').click(function() {
	if (numberA == null) {
		numberA = $('#txt-input-display').val();
		operation = 'Subtraction';
		$('#txt-input-display').val(null);
	}
	else if (numberB == null) {
		numberB = $('#txt-input-display').val();
		operation = 'Subtraction';
		numberB = null;
		$('#txt-input-display').val(null);
	}
});
$('#btn-multiply').click(function() {
	if (numberA == null) {
		numberA = $('#txt-input-display').val();
		operation = 'Multiplication';
		$('#txt-input-display').val(null);
	}
	else if (numberB == null) {
		numberB = $('#txt-input-display').val();
		operation = 'Multiplication';
		numberB = null;
		$('#txt-input-display').val(null);
	}
});
$('#btn-divide').click(function() {
	if (numberA == null) {
		numberA = $('#txt-input-display').val();
		operation = 'Division';
		$('#txt-input-display').val(null);
	}
	else if (numberB == null) {
		numberB = $('#txt-input-display').val();
		operation = 'Division';
		numberB = null;
		$('#txt-input-display').val(null);
	}
});
$('#btn-equal').click(function () {
	if(numberB == null && $('#txt-input-display').val() != null) {
		numberB = $('#txt-input-display').val();
	}
	if(operation == 'Addition') {
		$('#txt-input-display').val(parseFloat(numberA) + parseFloat(numberB));
	}
	if(operation == 'Subtraction') {
		$('#txt-input-display').val(parseFloat(numberA) - parseFloat(numberB));
	}
	if(operation == 'Multiplication') {
		$('#txt-input-display').val(parseFloat(numberA) * parseFloat(numberB));
	}
	if(operation == 'Division') {
		$('#txt-input-display').val(parseFloat(numberA) / parseFloat(numberB));
	}

});

$('#btn-clear-all').click( function(){
	numberA = null;
	numberB = null;
	operation = null;
	$('#txt-input-display').val(null);

});


$('#btn-backspace').click( function(){
	let txtInputDisplay = $('#txt-input-display').val();
	$('#txt-input-display').val(txtInputDisplay.substr(0, txtInputDisplay.length-1));
});

$('#btn-decimal').click( function(){
	let txtInputDisplay = $('#txt-input-display').val();
	if(txtInputDisplay.includes(".")) {
		return;
	}
	$('#txt-input-display').val(txtInputDisplay+='.');
});






